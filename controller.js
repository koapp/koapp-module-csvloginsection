(function() {
  'use strict';

  angular
    .module('csvloginsection', [])
    .controller('csvloginsectionController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location', '$rootScope', '$http', '$q', '$window'];

  function loadFunction($scope, structureService, $location, $rootScope, $http, $q, $window) {
    //Register upper level modules
    structureService.registerModule($location, $scope, 'csvloginsection');

    // --- Start apiconectionController content ---
    structureService.launchSpinner('.holds-the-iframe');
    $rootScope.isBusy  = true;
   
    let config = $scope.csvloginsection.modulescope;
    let moduleIndex = "csvLoginData";//structureService.getIndex().split("/").slice(-1)[0];

    $scope.config = config;
        

    if(config.urlData === "") errorManager("Missing Data");
    //config.page = "Hoja 1";
    const apiId =  config.googleApiKey; //"AIzaSyBAGbtyetQ_MneMt25uEptQQQWiDtDF4PM";
    
    //get the csv from the config or from a url and call the init function

    if(localStorage.getItem(moduleIndex) && localStorage.getItem(moduleIndex) === "logedOut"){
        $scope.csvloginsection.useLogin = true;
        
        manageSheetRequests(config.loginfield.loginpage);
    }
    
    
    //define visualization type
    $scope.type = config.type;
    $scope.errorMessage = false; 
    $scope.itemDetail = false;
    $scope.detailData;
    $scope.embedModule = `${config.embedModule}?backUrl=${$location.url()} &&url=`;
    $scope.searchitem = $location.search().q;
    
    
    
    //manage sheet requests
    function manageSheetRequests(page){
        let parsedData = [];
        
        //get the sheet id from the url
        let sheetId= config.urlData.split("/");
        sheetId = sheetId[sheetId.findIndex((element) => element === "d" )+1];
      
        //fix sheet page
        if(!page){errorManager("Missing page in the config"); return}
        page = encodeURIComponent(page);
      
        const url = `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${page}?majorDimension=ROWS&&key=${apiId}`;
        
        //build the request
        var req = {
            method: 'GET',
            url: url
        }
        $http(req).then((result) => parseData(result));
        
        function parseData(result){
            let items = result.data.values;
            for(let i = 1; i < items.length; i++){
                parsedData.push({});
                
                for(let y = 0; y < items[0].length; y++){
                    let index = items[0][y];
                    let value = items[i][y];
                    parsedData[i-1][index] = value;
                    
                }
            }
            $scope.loginData = parsedData;
        }
    }
    
    //catch login form submit and chech user info
    $scope.csvloginsectionManage = function(){
        console.log("check login csvloginsection", $scope.loginData, $scope.csvloginsection.csvLoginiName, $scope.csvloginsection.csvLoginiPass);
        let userIndex =  $scope.loginData.findIndex((elm)=>{ return elm[config.loginfield.login] === $scope.csvloginsection.csvLoginiName });
        if(userIndex >= 0 &&  $scope.loginData[userIndex][config.loginfield.pass] === $scope.csvloginsection.csvLoginiPass){
            console.log("login correcto");
            
            let filter = {
                field: config.loginfield.id,
                userValue:  $scope.loginData[userIndex][config.loginfield.id] 
            }
            
            
            
            localStorage.setItem(moduleIndex, JSON.stringify(filter));
            
            
            $scope.csvloginsection.useLogin = false;
            
        }else{
            console.log("user or password are wrong");
        }
    }
  }
    
}());
