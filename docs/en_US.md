# **(How to) Configurar el módulo CSV Draw**

Este módulo te permite utilizar un archivo CSV, subido en google Sheets, y utilizarlo para mostrar información en tu app. Una vez configurado podrás compilar tu app y todos los cambios realizados en el CSV se verán reflejados en tu app sin la necesidad de recompilarla.

  

## **Como configurarlo**

  

Puedes utilizar este módulo de dos formas.

Mostrar contenido estático (como un menú o un listado). Si ese es el caso, puedes emplear el campo:

**Type your csv here**

Si deseas mostrar datos de forma dinámica puedes utilizar el campo:

**Url of your csv file**

para pegar la url de tu archivo de Google sheets.

Para que el modulo funcione correctamente tu archivo de google sheets debe ser publico via url. 

 - Abre tu google sheet y haz click en el botón **Compartir** en la esquina superior derecha.
 ![enter image description here](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/csvdraw/shareButton.png)
 - Has click en **Cualquier persona con el enlace**
 ![enter image description here](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/csvdraw/shareFileUrl.png)
- Ahora tu archivo de google sheets está listo para ser utilizado por el módulo. Copia la url de tu hoja de calculo y pégala en el campo **Url of your csv file**

Además del acceso a travez de url, google requiere un id de api para que aplicaciones externas accedan a tu archivo. Para conseguir la tuya sigue estos pasos:

 - Abre la consola de [google apis](https://console.developers.google.com/) con tu cuenta de google
 - Acepta los términos y condiciones. 
 - Haz click en **Crear proyecto**
 - Dale un nombre a tu proyecto.
 - Accede al apartado **Credenciales** y haz click en el botón **+ Crear Credenciales** y luego en **Clave Api**
 - Ya tienes lista tu api key, sin embargo es buena idea restringir el acceso solo para aquellas características que necesitas utilizar
 - (Opcional) Has click en **Restringir api**  y marca la opción **Restringir clave** y selecciona la api de google sheets. (si no aparece la clave, es posible que debas agregarla en la sección biblioteca)
 
 Agrega el nombre de de la hoja que quieres utilizar en el campo **The page of your Google sheets**. 
 A continuación, selecciona la forma en la que se visualizarán tus datos y rellena el resto de campos utilizando los nombres de cada columna de tu csv (promera fila del google sheet)
 
